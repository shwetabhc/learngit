# Localizing the Plugin

- Generate .POT file. Use command: WP CLI: wp i18n make-pot . languages/hotel-spider.pot
- Translate .POT File and save as hotel-spider-{locale}.po for e.g. hotel-spider-de_DE.po
- Genrate .MO file from .PO file and save as hotel-spider-{locale}.mo. Use command: msgfmt -o hotel-spider-{locale}.mo hotel-spider-{locale}.po
- Resource: (more info [here](https://developer.wordpress.org/plugins/internationalization/localization/))
- WP CLI Resource: (more info [here](https://developer.wordpress.org/cli/commands/i18n/make-pot/)