jQuery(document).ready(function(){

    var hsbeValidatedAdults = 1;
    var hsbeValidatedChildren = 1;
    var hsbeValidatedInfants = 1;
    jQuery("form").submit(function(e){
        if(jQuery("#hsbe_adults_min").children("option:selected").val()>jQuery("#hsbe_adults_max").children("option:selected").val()){
            jQuery("#hsbe-validation-error").removeClass("hsbe-error-msg-hide");
            jQuery("#adult-max").removeClass("hsbe-error-msg-hide");
            jQuery(window).scrollTop(0);
            hsbeValidatedAdults = 0;
        }
        else{
            jQuery("#adult-max").addClass("hsbe-error-msg-hide");
            hsbeValidatedAdults = 1;
        }
        if(jQuery("#hsbe_children_min").children("option:selected").val()>jQuery("#hsbe_children_max").children("option:selected").val()){
            jQuery("#hsbe-validation-error").removeClass("hsbe-error-msg-hide");
            jQuery("#children-max").removeClass("hsbe-error-msg-hide");
            jQuery(window).scrollTop(0);
            hsbeValidatedChildren = 0;
        }
        else{
            jQuery("#children-max").addClass("hsbe-error-msg-hide");
            hsbeValidatedChildren = 1;
        }
        if(jQuery("#hsbe_infants_min").children("option:selected").val()>jQuery("#hsbe_infants_max").children("option:selected").val()){
            jQuery("#hsbe-validation-error").removeClass("hsbe-error-msg-hide");
            jQuery("#infants-max").removeClass("hsbe-error-msg-hide");
            jQuery(window).scrollTop(0);
            hsbeValidatedInfants = 0;
        }
        else{
            jQuery("#infants-max").addClass("hsbe-error-msg-hide");
            hsbeValidatedInfants = 1;
        }
        if(!(hsbeValidatedAdults&&hsbeValidatedChildren&&hsbeValidatedInfants)){
            e.preventDefault();
        }
    });

});