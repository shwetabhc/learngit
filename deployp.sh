#! /bin/bash

echo "Installing subversion"
apt-get install -y subversion
echo "After installing subversion"

# Main configuration
PLUGINSLUG="hotel-spider"
CURRENTDIR=`pwd`
MAINFILE="hs-direct-booking.php" # Main php file of the plugin
TRUNKCMTMSG="Pushing files to trunk"

# Git configuration
GITPATH="$CURRENTDIR/" # This file should be in the base of git repo

# Svn configuration
SVNPATH="/tmp/$PLUGINSLUG" # Path to a temp SVN repo. No trailing slash required and don't add trunk.
SVNURL="https://grey-hatch.svn.beanstalkapp.com/greyhatch/" # Remote SVN repo on wordpress.org, with trailing slash
SVNUSER="shwetabhc" # Svn username
SVNPASS=$SVNREPOPASS


# Start the work...
echo ".........................................."
echo 
echo "Preparing to deploy wordpress plugin"
echo 
echo ".........................................."
echo

# Check if subversion is installed before getting all worked up
if ! which svn >/dev/null; then
	echo "You'll need to install subversion before proceeding. Exiting....";
	exit 1;
fi

# Check version in readme.txt is the same as plugin file after translating both to unix line breaks to work around grep's failure to identify mac line breaks
NEWVERSION1=`grep "^Stable tag:" $GITPATH/readme.txt | awk -F' ' '{print $NF}'`
echo "readme.txt version: $NEWVERSION1"
NEWVERSION2=`grep "Version:" $GITPATH/$MAINFILE | awk -F' ' '{print $NF}'`
echo "$MAINFILE version: $NEWVERSION2"

if [ "$NEWVERSION1" != "$NEWVERSION2" ]; then echo "Version in readme.txt & $MAINFILE don't match. Exiting...."; exit 1; fi

cd $GITPATH

echo "Creating local copy of SVN repo ..."
svn co --username=$SVNUSER --password=$SVNPASS $SVNURL $SVNPATH

echo "Exporting the HEAD of master from git to the trunk of SVN"
git checkout-index -a -f --prefix=$SVNPATH/trunk/

echo "Ignoring github specific files and deployment script"
svn propset svn:ignore "deploy.sh
README.md
.git
.gitignore" "$SVNPATH/trunk/"

echo "Changing directory to SVN and committing to trunk"
cd $SVNPATH/trunk/
# Add all new files that are not set to be ignored
svn status | grep -v "^.[ \t]*\..*" | grep "^?" | awk '{print $2}' | xargs svn add
svn commit --username=$SVNUSER --password=$SVNPASS -m "$TRUNKCMTMSG"

echo "Creating new SVN tag & committing it"
cd $SVNPATH
svn copy trunk/ tags/$NEWVERSION1/
cd $SVNPATH/tags/$NEWVERSION1
svn commit --username=$SVNUSER --password=$SVNPASS -m "Tagging version $NEWVERSION1"

echo "Removing temporary directory $SVNPATH"
rm -fr $SVNPATH/

echo "*** FINISHED ***"