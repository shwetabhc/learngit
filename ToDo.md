# List of things to be done:

- [X] Create a Git repository on Hotel-Spider's Bitbucket account
- [ ] Define the product name ("HS Booking Engine") as a unique constant
- [ ] [Translations] Add a script to generate the POT file (more info [here](https://developer.wordpress.org/plugins/internationalization/localization/))
- [ ] [Translations] Load translations from PO files
- [ ] Add a Markdown file to this repo describing how to publish a new version of the plugin
- [X] Add a new field in the configuration to set the default values (`initial` parameter) in the guest fields (for adults, children and infants)
- [ ] Add support for all parameters on guest fields (for adults, children and infants): `display`, `initial` (already done), `min` and `max`
- [ ] Add a branch flag (Staging | Production)
