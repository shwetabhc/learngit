<div class="wrap">
	<h1><?php _e('Hotel-Spider Booking Engine Settings', 'hotel-spider'); ?></h1>
	<div class="hsbe-error-msg-hide" id="hsbe-validation-error">
		<p class="hsbe-error-msg hsbe-error-msg-hide" id="adult-max"><?php _e('Adults minimum value is greater than maximum value', 'hotel-spider'); ?></p>
		<p class="hsbe-error-msg hsbe-error-msg-hide" id="children-max"><?php _e('Children minimum value is greater than maximum value', 'hotel-spider'); ?></p>
		<p class="hsbe-error-msg hsbe-error-msg-hide" id="infants-max"><?php _e('Infants minimum value is greater than maximum value', 'hotel-spider'); ?></p>		
	</div>
	<?php settings_errors(); ?>

	<form action="options.php" method="post">
		<?php
		settings_fields('hs_booking_engine_configuration');
		do_settings_sections('hs_booking_engine_settings');
		submit_button();
		?>
	</form>
</div>