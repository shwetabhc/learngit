# Publishing new version of plugin

- WordPress uses SVN (subversion) for publishing plugins
- Install SVN and create a local directory
- Checkout SVN repo on wordpress. Use command: svn co https://plugins.svn.wordpress.org/hotel-spider my-local-dir
- Commit changes in trunk directory. Use command: svn ci -m "your-comment"
- For new version create directory inside tag with version name, for example: tags/1.5
- Copy content from trunk to tags/{version}
- At last check in changes using command: svn ci -m "Check in comment"
- Resource: (more info [here](https://developer.wordpress.org/plugins/wordpress-org/how-to-use-subversion/))
